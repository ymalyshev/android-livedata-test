package com.example.myapplication.domain.developers

class Developer(
    val id: Int,
    val firstName: String,
    val lastName: String)