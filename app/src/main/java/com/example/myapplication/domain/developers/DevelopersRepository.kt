package com.example.myapplication.domain.developers

import androidx.lifecycle.LiveData

interface DevelopersRepository {

    val developers: LiveData<List<Developer>>

    val observersCount: Int
}