package com.example.myapplication.data.developers

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.myapplication.domain.developers.Developer
import com.example.myapplication.domain.developers.DevelopersRepository

class DevelopersRepositoryImpl: DevelopersRepository {

    override val developers: LiveData<List<Developer>> by lazy { createDevelopersData() }

    private fun createDevelopersData() = MutableLiveData<List<Developer>>().also {
        it.value = listOf(
            Developer(1, "Yevgeniy", "Malyshev"),
            Developer(2, "Mykola", "Podolian"),
            Developer(3, "Ihor", "Bezeka"),
            Developer(4, "Oleksii", "Yur")
        )
    }

    override val observersCount: Int
        get() {
            val clazz = LiveData::class.java
            val observersField = clazz.getDeclaredField("mObservers")
            observersField.isAccessible = true
            val observers = observersField.get(developers)
            val observersClass = observers.javaClass
            val sizeField = observersClass.getDeclaredField("mSize");
            sizeField.isAccessible = true
            return sizeField.getInt(observers)
        }
}