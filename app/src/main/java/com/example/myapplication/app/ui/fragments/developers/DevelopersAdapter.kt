package com.example.myapplication.app.ui.fragments.developers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.domain.developers.Developer

class DevelopersAdapter(context: Context): RecyclerView.Adapter<DevelopersAdapter.DeveloperViewHolder>() {

    private val inflater = LayoutInflater.from(context)
    private val listener = context as? OnDeveloperClickListener
    private var developers: List<Developer> = emptyList()

    fun swapDevelopers(developers: List<Developer>) {
        this.developers = developers
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DeveloperViewHolder {
        val view = inflater.inflate(R.layout.item_developer, parent, false)
        return DeveloperViewHolder(view, listener)
    }

    override fun getItemCount(): Int = developers.size

    override fun onBindViewHolder(holder: DeveloperViewHolder, position: Int) {
        holder.bind(developers[position])
    }

    fun getItem(position: Int) = developers[position]

    class DeveloperViewHolder(view: View, private val listener: OnDeveloperClickListener?): RecyclerView.ViewHolder(view) {

        private val textView: TextView = view.findViewById(R.id.text)
        private var developer: Developer? = null

        init {
            textView.setOnClickListener { _ ->
                developer?.let { listener?.onDeveloperClick(it) }
            }
        }

        fun bind(developer: Developer) {
            this.developer = developer
            textView.text = "${developer.firstName} ${developer.lastName}"
        }
    }
}