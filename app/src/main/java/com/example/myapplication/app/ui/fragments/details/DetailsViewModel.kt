package com.example.myapplication.app.ui.fragments.details

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import com.example.myapplication.app.App
import com.example.myapplication.app.util.filterNotNull
import com.example.myapplication.domain.developers.DevelopersRepository

class DetailsViewModel(application: Application): AndroidViewModel(application) {

    private val developersRepository: DevelopersRepository
    private var developerNameData: LiveData<String>? = null

    init {
        val app = application as App
        developersRepository = app.developersRepository
    }

    fun getDeveloperName(id: Int): LiveData<String> {
        developerNameData?.let { return it }
        return developersRepository.developers
            .map { it.find { developer -> developer.id == id } }
            .filterNotNull()
            .map { it.firstName }.also { developerNameData = it }
    }
}