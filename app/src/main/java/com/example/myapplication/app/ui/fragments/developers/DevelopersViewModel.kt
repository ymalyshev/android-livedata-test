package com.example.myapplication.app.ui.fragments.developers

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.example.myapplication.app.App
import com.example.myapplication.domain.developers.Developer
import com.example.myapplication.domain.developers.DevelopersRepository

class DevelopersViewModel(application: Application): AndroidViewModel(application) {

    private val developersRepository: DevelopersRepository

    init {
        val app = application as App
        developersRepository = app.developersRepository
    }

    val developersData: LiveData<List<Developer>>
        get() = developersRepository.developers
}