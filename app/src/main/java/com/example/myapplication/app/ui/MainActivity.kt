package com.example.myapplication.app.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.myapplication.R
import com.example.myapplication.app.App
import com.example.myapplication.app.ui.fragments.details.DetailsFragment
import com.example.myapplication.app.ui.fragments.developers.DevelopersFragment
import com.example.myapplication.app.ui.fragments.developers.OnDeveloperClickListener
import com.example.myapplication.domain.developers.Developer
import com.example.myapplication.domain.developers.DevelopersRepository

class MainActivity : AppCompatActivity(), OnDeveloperClickListener {

    private val developersRepository: DevelopersRepository by lazy {
        (applicationContext as App).developersRepository
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (supportFragmentManager.findFragmentById(R.id.container) == null) {
            supportFragmentManager.beginTransaction()
                .add(R.id.container, DevelopersFragment())
                .commit()
        }
        findViewById<Button>(R.id.count_observers).setOnClickListener {
            val count = developersRepository.observersCount
            Toast.makeText(this, getString(R.string.observers_count_message, count), Toast.LENGTH_LONG).show()
        }
        findViewById<Button>(R.id.empty_frame).setOnClickListener {
            openFrame(Fragment())
        }
    }

    override fun onDeveloperClick(developer: Developer) {
        openFrame(DetailsFragment.newInstance(developer.id))
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount == 0) {
            super.onBackPressed()
        } else {
            supportFragmentManager.popBackStack()
        }
    }

    private fun openFrame(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, fragment)
            .addToBackStack(null)
            .commit()
    }
}
