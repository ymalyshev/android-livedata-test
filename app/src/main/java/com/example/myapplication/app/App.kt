package com.example.myapplication.app

import android.app.Application
import com.example.myapplication.data.developers.DevelopersRepositoryImpl

class App: Application() {

    val developersRepository: DevelopersRepositoryImpl by lazy { DevelopersRepositoryImpl() }
}