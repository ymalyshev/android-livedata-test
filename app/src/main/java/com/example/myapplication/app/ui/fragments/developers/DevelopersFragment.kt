package com.example.myapplication.app.ui.fragments.developers

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R

class DevelopersFragment: Fragment() {

    private val viewModel: DevelopersViewModel by lazy { createViewModel() }
    private var adapter: DevelopersAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_developers, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecycler(view)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        adapter = null
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.developersData.observe(viewLifecycleOwner, Observer {
            adapter?.swapDevelopers(it)
        })
    }

    private fun setupRecycler(view: View) {
        val context = requireContext()

        val recyclerView: RecyclerView = view.findViewById(R.id.recycler)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        adapter = DevelopersAdapter(context)
        recyclerView.adapter = adapter
    }

    private fun createViewModel(): DevelopersViewModel {
        return ViewModelProviders.of(this)
            .get(DevelopersViewModel::class.java)
    }
}