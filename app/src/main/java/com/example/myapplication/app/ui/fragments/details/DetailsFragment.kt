package com.example.myapplication.app.ui.fragments.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.myapplication.R

private const val ARG_DEVELOPER_ID = "developer_id"

class DetailsFragment: Fragment() {

    private val viewModel: DetailsViewModel by lazy { createViewModel() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_details, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.getDeveloperName(arguments.developerId).observe(viewLifecycleOwner, Observer {
            setupDeveloperName(it)
        })
    }

    private fun setupDeveloperName(name: String) {
        val textView: TextView = requireView().findViewById(R.id.text)
        textView.text = getString(R.string.hello_message, name)
    }

    private fun createViewModel() = ViewModelProviders
        .of(this).get(DetailsViewModel::class.java)

    companion object {

        fun newInstance(id: Int) = DetailsFragment().also {
            val args = Bundle()
            args.putInt(ARG_DEVELOPER_ID, id)
            it.arguments = args
        }
    }
}

private val Bundle?.developerId: Int
    get() {
        if (this == null || !this.containsKey(ARG_DEVELOPER_ID)) {
            throw IllegalArgumentException("No argument $ARG_DEVELOPER_ID found")
        }
        return this.getInt(ARG_DEVELOPER_ID)
    }