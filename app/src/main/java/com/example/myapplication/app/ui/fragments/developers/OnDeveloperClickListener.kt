package com.example.myapplication.app.ui.fragments.developers

import com.example.myapplication.domain.developers.Developer

interface OnDeveloperClickListener {

    fun onDeveloperClick(developer: Developer)
}