package com.example.myapplication.app.util

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData

fun <T> LiveData<T?>.filterNotNull(): LiveData<T> {
    val mediator = MediatorLiveData<T>()
    mediator.addSource(this) {
        it?.let { notNullValue -> mediator.value = notNullValue }
    }
    return mediator
}